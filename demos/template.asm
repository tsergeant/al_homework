;;
;; template.asm
;;
;; This is a suggested template for starting new NASM programs.
;; statements to high level languages.
;;
;; Author: Your Name
;; Version: Date
;;
%include "../lib/iomacros.asm"
%include "../lib/dumpregs.asm"

		global main


		section .data

endl:		db	10,0
message:	db	"Testing"


		section .bss


		section .text

main:
		align_stack		; always start code with this

		put_str	message
		put_str	endl




		mov     eax, 60		; always end code with this
		xor     rdi, rdi	; exit(0)
		syscall
